const URL = "https://api.mlab.com/api/1/databases/bootcamp/collections/posts?apiKey=50c5ea68e4b0a97d668bc84a";
var response;

function obtenerPosts () {
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response =JSON.parse(peticion.responseText);
  console.log(response);
  mostrarPosts();
}

function mostrarPosts() {
  var tabla= document.getElementById("tablaPosts");
  for (var i = 0; i < response.length; i++) {
    //alert(response[i].titulo);
    var elemento=response[i];

    var fila = tabla.insertRow(i+1);
    var celdaId = fila.insertCell(0);
    var celdaTitulo = fila.insertCell(1);
    var celdaTexto = fila.insertCell(2);
    var celdaAutor = fila.insertCell(3);
    var celdaOperaciones = fila.insertCell(4);

    celdaId.innerHTML = response[i]._id.$oid;
    celdaTitulo.innerHTML = elemento.titulo;
    celdaTexto.innerHTML = elemento.texto;
    if (elemento.autor != undefined) {
      celdaAutor.innerHTML = elemento.autor.nombre + " " + elemento.autor.apellido;
    } else {
      celdaAutor.innerHTML = "Anonimo";
    }
    celdaOperaciones.innerHTML = '<button onclick=\'actualizarPosts("' + celdaId.innerHTML + '")\'>Actualizar</button>';
    }

}

function anadirPosts () {
  var peticion = new XMLHttpRequest();
  peticion.open("POST", URL, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send('{"titulo":"PAB Nuevo POST desde Atom","texto":"Descripcion asociada al nuevo POST desde Atom ",  "autor":{"nombre":"Fernando", "apellidos":"Minguero" }}');
}

function actualizarPosts(id) {
  var peticion = new XMLHttpRequest();
  var URLItem = "https://api.mlab.com/api/1/databases/bootcamp/collections/posts";
  URLItem += id;
  URLItem += "?apiKey=50c5ea68e4b0a97d668bc84a";
  peticion.open("PUT", URL, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send('{"titulo":"PAB Cambio POST desde Atom"}');
}
