var expect = require('chai').expect;
var $ = require('chai-jquery');
var request = require('request');

describe("Pruebas sencillas",function(){
it('Test suma', function() {
  expect(9+4).to.equal(13);
  console.log("prueba completada");
});

it('Test internet', function(done) {
  request.get("http://www.forocoches.com",
              function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
  });
});
});

describe("Pruebas red",function(){
it('Test internet 2', function(done) {
  request.get("http://www.forocoches.com",
              function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
  });
});

it('Test internet 3', function(done) {
  request.get("http://localhost:8082/",
              function(error, response, body) {
                  expect(response.statusCode).to.equal(200);
                  done();
  });
});

it('Test body', function(done) {
  request.get("http://localhost:8082/",
              function(error, response, body) {
//                expect(body).to.equal('');
                  expect(body).contains('<h1>');
                  done();
  });
});
});

describe('Test contenido HTML',function(done){
  it('Test H1'), function(done){
    request.get("http://localhost:8082/",
      function(error, response, body) {
        expect($('body h1')).to.have.text("Bienvenido a mi blog");
        done();
      }
  )
  }
});
